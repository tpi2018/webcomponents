--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.6

-- Started on 2018-04-30 21:31:18 CST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 2396 (class 0 OID 16387)
-- Dependencies: 185
-- Data for Name: calendario; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2397 (class 0 OID 16393)
-- Dependencies: 186
-- Data for Name: calendario_excepcion; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO calendario_excepcion VALUES (3, '2018-06-26', true, 'Asueto nacional');
INSERT INTO calendario_excepcion VALUES (2, '2018-05-10', true, 'Dia de la Madre');
INSERT INTO calendario_excepcion VALUES (1, '2018-05-01', true, 'Dia del trabajaodr');
INSERT INTO calendario_excepcion VALUES (4, '2018-02-14', true, 'Dia libre');
INSERT INTO calendario_excepcion VALUES (5, '2018-04-06', true, 'Semana santa');
INSERT INTO calendario_excepcion VALUES (6, '2018-07-27', true, 'Fiestas');
INSERT INTO calendario_excepcion VALUES (7, '2018-08-05', true, 'Fiestas agosto');
INSERT INTO calendario_excepcion VALUES (8, '0218-07-15', true, 'Dia de la independencia');


--
-- TOC entry 2482 (class 0 OID 0)
-- Dependencies: 187
-- Name: calendario_excepcion_id_excepcion_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('calendario_excepcion_id_excepcion_seq', 8, true);


--
-- TOC entry 2483 (class 0 OID 0)
-- Dependencies: 188
-- Name: calendario_id_fecha_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('calendario_id_fecha_seq', 1, false);


--
-- TOC entry 2400 (class 0 OID 16403)
-- Dependencies: 189
-- Data for Name: diagnostico; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2484 (class 0 OID 0)
-- Dependencies: 190
-- Name: diagnostico_id_diagnostico_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('diagnostico_id_diagnostico_seq', 1, false);


--
-- TOC entry 2402 (class 0 OID 16411)
-- Dependencies: 191
-- Data for Name: diagnostico_parte; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2485 (class 0 OID 0)
-- Dependencies: 192
-- Name: diagnostico_parte_id_diagnostico_parte_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('diagnostico_parte_id_diagnostico_parte_seq', 1, false);


--
-- TOC entry 2404 (class 0 OID 16416)
-- Dependencies: 193
-- Data for Name: equipo; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2405 (class 0 OID 16422)
-- Dependencies: 194
-- Data for Name: equipo_detalle; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2486 (class 0 OID 0)
-- Dependencies: 195
-- Name: equipo_detalle_id_equipo_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('equipo_detalle_id_equipo_detalle_seq', 1, false);


--
-- TOC entry 2487 (class 0 OID 0)
-- Dependencies: 196
-- Name: equipo_id_equipo_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('equipo_id_equipo_seq', 1, false);


--
-- TOC entry 2408 (class 0 OID 16432)
-- Dependencies: 197
-- Data for Name: equipo_parte; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2409 (class 0 OID 16435)
-- Dependencies: 198
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO estado VALUES (1, 'Estado 1', true, 'Descripcion');
INSERT INTO estado VALUES (2, 'Estado 2', false, 'Descripcion 2');


--
-- TOC entry 2488 (class 0 OID 0)
-- Dependencies: 199
-- Name: estado_id_estado_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('estado_id_estado_seq', 2, true);


--
-- TOC entry 2411 (class 0 OID 16443)
-- Dependencies: 200
-- Data for Name: estado_mantenimiento_detalle; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2489 (class 0 OID 0)
-- Dependencies: 201
-- Name: estado_mantenimiento_detalle_id_estado_mantenimiento_detall_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('estado_mantenimiento_detalle_id_estado_mantenimiento_detall_seq', 1, false);


--
-- TOC entry 2413 (class 0 OID 16448)
-- Dependencies: 202
-- Data for Name: mantenimiento_detalle; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2490 (class 0 OID 0)
-- Dependencies: 203
-- Name: mantenimiento_detalle_id_mantenimiento_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('mantenimiento_detalle_id_mantenimiento_detalle_seq', 1, false);


--
-- TOC entry 2415 (class 0 OID 16453)
-- Dependencies: 204
-- Data for Name: marca; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO marca VALUES (2, 'Asus', true, 'Marca Asus');
INSERT INTO marca VALUES (3, 'Dell', true, 'Marca Dell');
INSERT INTO marca VALUES (4, 'Acer', false, 'Marca Acer');
INSERT INTO marca VALUES (5, 'Toshiba', true, 'Marca Toshiba');
INSERT INTO marca VALUES (1, 'HP', false, 'Marca Hp');
INSERT INTO marca VALUES (6, 'Tactix', true, 'Marca de herramientas electronicas');
INSERT INTO marca VALUES (7, 'Mega', true, 'Marca de controles');
INSERT INTO marca VALUES (8, 'RadioShack', true, 'Multimedia electronica digital');
INSERT INTO marca VALUES (9, 'Samsung', true, 'Marca samsung');
INSERT INTO marca VALUES (10, 'Sony', false, 'Marca Sony');
INSERT INTO marca VALUES (11, 'Apple', true, 'Marca Apple');


--
-- TOC entry 2491 (class 0 OID 0)
-- Dependencies: 205
-- Name: marca_id_marca_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('marca_id_marca_seq', 11, true);


--
-- TOC entry 2417 (class 0 OID 16461)
-- Dependencies: 206
-- Data for Name: modelo; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO modelo VALUES (1, 'Modelo 1', true, 'Des');
INSERT INTO modelo VALUES (2, 'Modelo 2', true, 'Des');


--
-- TOC entry 2492 (class 0 OID 0)
-- Dependencies: 207
-- Name: modelo_id_modelo_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('modelo_id_modelo_seq', 2, true);


--
-- TOC entry 2419 (class 0 OID 16469)
-- Dependencies: 208
-- Data for Name: orden; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2420 (class 0 OID 16475)
-- Dependencies: 209
-- Data for Name: orden_calendario; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2493 (class 0 OID 0)
-- Dependencies: 210
-- Name: orden_calendario_id_orden_calendario_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('orden_calendario_id_orden_calendario_seq', 1, false);


--
-- TOC entry 2494 (class 0 OID 0)
-- Dependencies: 211
-- Name: orden_id_orden_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('orden_id_orden_seq', 1, false);


--
-- TOC entry 2423 (class 0 OID 16482)
-- Dependencies: 212
-- Data for Name: orden_trabajo_equipo; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2424 (class 0 OID 16485)
-- Dependencies: 213
-- Data for Name: orden_trabajo_equipo_detalle; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2495 (class 0 OID 0)
-- Dependencies: 214
-- Name: orden_trabajo_equipo_detalle_id_orden_trabajo_equipo_detall_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('orden_trabajo_equipo_detalle_id_orden_trabajo_equipo_detall_seq', 1, false);


--
-- TOC entry 2496 (class 0 OID 0)
-- Dependencies: 215
-- Name: orden_trabajo_equipo_id_orden_trabajo_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('orden_trabajo_equipo_id_orden_trabajo_seq', 1, false);




--
-- TOC entry 2429 (class 0 OID 16500)
-- Dependencies: 218
-- Data for Name: paso; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2498 (class 0 OID 0)
-- Dependencies: 219
-- Name: paso_id_paso_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('paso_id_paso_seq', 1, false);


--
-- TOC entry 2431 (class 0 OID 16508)
-- Dependencies: 220
-- Data for Name: prioridad; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO prioridad VALUES (1, 'Prioridad 1', 'descripcion', true);


--
-- TOC entry 2499 (class 0 OID 0)
-- Dependencies: 221
-- Name: prioridad_id_prioridad_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('prioridad_id_prioridad_seq', 1, true);


--
-- TOC entry 2433 (class 0 OID 16516)
-- Dependencies: 222
-- Data for Name: procedimiento; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2500 (class 0 OID 0)
-- Dependencies: 223
-- Name: procedimiento_id_procedimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('procedimiento_id_procedimiento_seq', 1, false);


--
-- TOC entry 2435 (class 0 OID 16524)
-- Dependencies: 224
-- Data for Name: responsable; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2501 (class 0 OID 0)
-- Dependencies: 225
-- Name: responsable_id_responsable_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('responsable_id_responsable_seq', 1, false);


--
-- TOC entry 2437 (class 0 OID 16532)
-- Dependencies: 226
-- Data for Name: solicitud; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2502 (class 0 OID 0)
-- Dependencies: 227
-- Name: solicitud_id_solicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('solicitud_id_solicitud_seq', 1, false);


--
-- TOC entry 2439 (class 0 OID 16537)
-- Dependencies: 228
-- Data for Name: sub_tipo_mantenimiento; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2503 (class 0 OID 0)
-- Dependencies: 229
-- Name: sub_tipo_mantenimiento_id_sub_tipo_mantenimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('sub_tipo_mantenimiento_id_sub_tipo_mantenimiento_seq', 1, false);


--
-- TOC entry 2441 (class 0 OID 16545)
-- Dependencies: 230
-- Data for Name: tipo_mantenimiento; Type: TABLE DATA; Schema: public; Owner: mortal2018
--



--
-- TOC entry 2504 (class 0 OID 0)
-- Dependencies: 231
-- Name: tipo_mantenimiento_id_tipo_mantenimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('tipo_mantenimiento_id_tipo_mantenimiento_seq', 1, false);


--
-- TOC entry 2443 (class 0 OID 16553)
-- Dependencies: 232
-- Data for Name: tipo_parte; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO tipo_parte VALUES (1, 'red', 'Partes para red');
INSERT INTO tipo_parte VALUES (2, 'almacenamiento', 'Partes para almacenamiento de datos');
INSERT INTO tipo_parte VALUES (3, 'extras', 'Extras');
INSERT INTO tipo_parte VALUES (4, 'hardware', 'Partes de hardware');
INSERT INTO tipo_parte VALUES (5, 'Unidades de lectura', 'Partes para aunidades de lectura');
INSERT INTO tipo_parte VALUES (6, 'Unidades de entrada', 'Partes para unidades de entrada');
INSERT INTO tipo_parte VALUES (7, 'Unidades de salida', 'Partes para unidades de salida');
INSERT INTO tipo_parte VALUES (8, 'Enfriamiento', 'Partes para enfriamiento');


--
-- TOC entry 2505 (class 0 OID 0)
-- Dependencies: 233
-- Name: tipo_parte_id_tipo_parte_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('tipo_parte_id_tipo_parte_seq', 8, true);

--
-- TOC entry 2427 (class 0 OID 16492)
-- Dependencies: 216
-- Data for Name: parte; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO parte VALUES (1, 'Router', 'Router', 1);
INSERT INTO parte VALUES (2, 'Disco duro', 'Disco duro', 2);
INSERT INTO parte VALUES (3, 'Mouse', 'Mouse', 6);
INSERT INTO parte VALUES (4, 'Teclado', 'Teclado', 6);
INSERT INTO parte VALUES (5, 'Camara web', 'camara', 3);
INSERT INTO parte VALUES (6, 'Monitor', 'monitor', 7);
INSERT INTO parte VALUES (7, 'Impresora', 'impresora', 7);
INSERT INTO parte VALUES (8, 'Scanner', 'scanner', 6);
INSERT INTO parte VALUES (9, 'Lector de discos', 'lector cd/dvd', 6);
INSERT INTO parte VALUES (10, 'RAM', 'ram', 4);
INSERT INTO parte VALUES (11, 'Ventilador', 'ventilador', 8);


--
-- TOC entry 2497 (class 0 OID 0)
-- Dependencies: 217
-- Name: parte_id_parte_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('parte_id_parte_seq', 11, true);

--
-- TOC entry 2445 (class 0 OID 16561)
-- Dependencies: 234
-- Data for Name: tipo_responsable; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO tipo_responsable VALUES (1, 'Tipo Responsable 1', 'Descripcion de tipo responsable 1');
INSERT INTO tipo_responsable VALUES (2, 'Tipo Responsable 2', 'Descripcion de tipo responsable 2');
INSERT INTO tipo_responsable VALUES (3, 'Tipo Respobsale 3', 'Descripcion de tipo responsable 3');
INSERT INTO tipo_responsable VALUES (4, 'Prueba', 'Descripcion de prueba');
INSERT INTO tipo_responsable VALUES (5, 'Registro', 'Descripcion de registro');


--
-- TOC entry 2506 (class 0 OID 0)
-- Dependencies: 235
-- Name: tipo_responsable_id_tipo_responsable_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('tipo_responsable_id_tipo_responsable_seq', 5, true);


--
-- TOC entry 2447 (class 0 OID 16569)
-- Dependencies: 236
-- Data for Name: unidad; Type: TABLE DATA; Schema: public; Owner: mortal2018
--

INSERT INTO unidad VALUES (1, 'Ingenieria', '0001-200');
INSERT INTO unidad VALUES (2, 'Matematica', '0002-100');
INSERT INTO unidad VALUES (3, 'Ciencias Sociales', '0002-200');
INSERT INTO unidad VALUES (4, 'Profesorado', '0002-300');
INSERT INTO unidad VALUES (5, 'Idiomas', '0003-100');


--
-- TOC entry 2507 (class 0 OID 0)
-- Dependencies: 237
-- Name: unidad_id_unidad_seq; Type: SEQUENCE SET; Schema: public; Owner: mortal2018
--

SELECT pg_catalog.setval('unidad_id_unidad_seq', 5, true);


-- Completed on 2018-04-30 21:31:19 CST

--
-- PostgreSQL database dump complete
--


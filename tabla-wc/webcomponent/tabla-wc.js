class TablaGenerica extends HTMLElement {
    constructor() {
        super();
        this.root = this.attachShadow({ mode: 'closed' });
        this.root.innerHTML = `
        <style>
            table, td, th {
                border: 1px solid #ddd;
                text-align: left;
            }

            table {
                border-collapse: collapse;
                width: 100%;
            }

            th, td {
                text-align: center;
                padding: 15px;
            }
            tr:hover {
                background: rgba(76, 175, 80, 0.1);
            }
            button {
              position:relative;
              left:25%;
              background-color: #4CAF50; /* Green */
              border: none;
              margin-top: 1vw;
              margin-left: 5vw;
              margin-right: 5vw;
              color: white;
              padding: 1vw 2.5vw;
              text-align: center;
              text-decoration: none;
              display: inline-block;
              font-size: 16px;
            }

            button span {                
                cursor: pointer;
                display: inline-block;
                position: relative;
                transition: 0.5s;
            }
              
            .button-rigth span:after {
               content: '>';
               position: absolute;
               opacity: 0;
               top: 0;
               right: 20px;
               transition: 0.5s;
            }
              
            .button-rigth:hover span {
              padding-right: 1.5vw;
            }
              
            .button-rigth:hover span:after {
              opacity: 1;
              right: 0;
            }
            
            .button-left span:after {
                content: '<';
                position: absolute;
                opacity: 0;
                top: 0;
                left: 20px;
                transition: 0.5s;
             }
               
             .button-left:hover span {
               padding-left: 1.5vw;
             }
               
             .button-left:hover span:after {
               opacity: 1;
               left: 0;
             }
            
            
            select {
              position: relative;
              left:25%;
              padding: 0.5vw 1.5vw;
            }
        </style>
        `;
    }

    get cabeceras() {
        return this.getAttribute("cabeceras");
    }

    get first() {
        return this.getAttribute("first");
    }

    get pagesize() {
        return this.getAttribute("pagesize");
    }

    get datos() {
        return this.getAttribute("datos");
    }

    connectedCallback() {
        //Hacer los elementos HTML necesarios
        let seleccionada;
        let tabla = document.createElement("table");
        let cabecerasArray = this.cabeceras.split(",");
        let select = document.createElement("select");
        let op = document.createElement("option");
        op.setAttribute("value",5);
        op.appendChild(document.createTextNode("5"));
        let op2 = document.createElement("option");
        op2.setAttribute("value",10);
        op2.appendChild(document.createTextNode("10"));
        select.appendChild(op);
        select.appendChild(op2);

        //Creacion del boton siguiente
        let btns = document.createElement("button");
        btns.setAttribute("class","button-rigth");
        let Aspa = document.createElement("span");        
        Aspa.innerText = "Siguiente ";
        btns.appendChild(Aspa);
        this.appendChild(btns);

        //Creacion del boton anterior
        let btna = document.createElement("button");
        btna.setAttribute("class","button-left");
        let Bspa = document.createElement("span");
        Bspa.innerText = " Anterior";
        btna.appendChild(Bspa);
        this.appendChild(btna);

        //Crear la tabla con sus encabezados
        let tr0 = document.createElement("tr");
        tr0.style.background = "rgba(76, 175, 80, 0.3)";
        tabla.appendChild(tr0);
        cabecerasArray.forEach(function (element) {
            let th = document.createElement("th");
            th.innerText = element.toUpperCase();
            th.setAttribute("draggable",true);
            th.style.width = 100/cabecerasArray.length+"%";
            tr0.appendChild(th);
        });
        this.tr0 = tr0;
        this.seleccionada = seleccionada;
        this.tabla = tabla;
        this.cabecerasArray = cabecerasArray;

        select.s = this;
        btns.s = this;
        btna.s = this;
        select.onchange=this.imprimir;
        btns.onclick = this.siguiente;
        btna.onclick = this.anterior;
        this.root.appendChild(tabla);
        this.root.appendChild(btna);
        this.root.appendChild(select);
        this.root.appendChild(btns);
    }

    anterior(){
        let comp = this.s;
        this.s.tabla.innerHTML="";
        this.s.tabla.appendChild(this.s.tr0);
        let f = comp.getAttribute("first");
        let pa = comp.getAttribute("pagesize");
        if((parseInt(f)-parseInt(pa))<0){
            comp.setAttribute("first",0);
        }else{
            comp.setAttribute("first",(parseInt(f)-parseInt(pa)));
        }
    }

    siguiente(){
        let comp = this.s;
        let f = comp.getAttribute("first");
        let pa = comp.getAttribute("pagesize");
        if (parseInt(f)+parseInt(pa) < JSON.parse(comp.datos).length) {
          comp.tabla.innerHTML="";
          comp.tabla.appendChild(this.s.tr0);
          comp.setAttribute("first",(parseInt(f)+parseInt(pa)));
        }//Else si se pasa
    }

    imprimir(){
        //console.log(this.value);
        let comp = this.s;
        //limpiar
        this.s.tabla.innerHTML="";
        this.s.tabla.appendChild(this.s.tr0);
        //agregar de nuevo
        comp.setAttribute("pagesize",this.value);
    }

    static get observedAttributes() {
        return ["first", "pagesize", "datos", "cabeceras"];
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (name == "datos") {
            this.crearFilas();
        }
        if(name=="pagesize"){
            this.crearFilas();
        }
        if(name=="first"){
            this.crearFilas();
        }
        if(name=="cabeceras"){
            this.crearFilas();
        }
    }

    crearFilas() {
        let seleccionada = this.seleccionada;
        let tr0 = this.tr0;
        let tabla = this.tabla;
        let cabecerasArray = this.cabeceras.split(",");
        if (this.datos != undefined && this.datos != "" && this.cabecerasArray != undefined) {
            tr0.innerHTML ="";
            cabecerasArray.forEach(function (element) {
                let th = document.createElement("th");
                th.innerText = element.toUpperCase();
                th.setAttribute("draggable",true);
                th.style.width = 100/cabecerasArray.length+"%";
                tr0.appendChild(th);
            });
            tabla.innerHTML="";
            tabla.appendChild(tr0);
            let d = JSON.parse(this.datos);
            let p = this.findRange(d, this.first, this.pagesize);
            //console.log(p);
            p.forEach(function (element) {
                let tr = document.createElement("tr");
                //Agregando evento cuando se seleciona fila
                tr.onclick = function () {
                    //Vaciar los colores
                    for (let fila of tabla.rows) {
                        fila.style.background = "";
                    }
                    tr0.style.background = "rgba(76, 175, 80, 0.3)";
                    //Guardar la seleccion
                    seleccionada = element;
                    this.style.background = "rgba(0,100,255,0.4)";
                    //console.log(seleccionada);
                }
                //Obtener atributos por cabeceras
                cabecerasArray.forEach(function (atri) {
                    let td = document.createElement("td");
                    td.setAttribute("contenteditable", "true");
                    td.innerText = element[atri];
                    td.onblur = function () {
                        //Verificar modificacion y hacer request update
                        if (seleccionada[atri] != this.innerText) {//Cambios
                            let temp = seleccionada;
                            temp[atri] = this.innerText;
                            //controlador.edit(temp).then(function (data) {
                            //    seleccionada = data;
                            //});
                            let event = new CustomEvent("tabla-cambio",{'composed':true,'bubbles':true,'detail':{'seleccionada':temp}});
                            this.dispatchEvent(event);
                        } else {//No hay cambios
                            //console.log("No hay Cambios");
                        }
                    }
                    tr.appendChild(td);
                });
                tabla.appendChild(tr);
            });
        }
        
    }

    findRange(objeto, f, pg){
        let lista = [];
        //console.log(objeto);
        if(objeto==null){
            return lista;
        }
        for(let i = parseInt(f); i<(parseInt(f)+parseInt(pg)); i++){
            if(objeto.length==i){
                break;
            }
            lista.push(objeto[i]);
        }
        return lista;
    }

}
customElements.define("tabla-wc", TablaGenerica);

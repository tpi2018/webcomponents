import AbstractController from "../controller/AbstractController.js";

let controlador = new AbstractController("marca");

controlador.findAll().then(function(data) {
  let tabla = document.querySelector("#idtabla");
  //console.log(tabla);
  tabla.setAttribute("datos",JSON.stringify(data));
});

document.addEventListener("tabla-cambio", function(e) {
    let seleccionada = e.detail.seleccionada;
    controlador.edit(seleccionada).then(function(data){
      seleccionada = data;
      //console.log(seleccionada);
      asignarDatos();
    });
});

function asignarDatos(){
  controlador.findAll().then(function(data) {
    let tabla = document.querySelector("#idtabla");
    //console.log(tabla);
    tabla.setAttribute("datos","");
    tabla.setAttribute("datos",JSON.stringify(data));
  });
}

asignarDatos();

import AbstractResource from './AbstractResource.js';
class AbstractInterface extends AbstractResource {
    constructor(entidad){
        super();
        this.entidad=entidad;
        this._url+=this.entidad;
    }

    findByName(nombre){
        return fetch(this._url+"/findbyname/"+nombre);
    }

    findAll(){
        return fetch(this._url+"/all");
    }

    findRange(first, pagesize){
        return fetch(this._url+"?first="+first+"&pagesize="+pagesize);
    }

    count(){
        return fetch(this._url+"/count");
    }

    findById(id){
        return fetch(this._url+"/"+id);
    }

    //El REST create, edit y delete (NO HACEN GET)
    edit(entidad){
        return fetch(this._url,{
            method: 'PUT',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify(entidad)
        });
    }

}
export default AbstractInterface;

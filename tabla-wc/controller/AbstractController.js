import AbstractInterface from '../boundary/AbstractInterface.js';
class AbstractController{
  constructor(entidad){
    //Usar Nuestro AbstractInterface
    this.interfaz = new AbstractInterface(entidad);
  }

  findByName(nombre){
    return this.interfaz.findByName(nombre).then(function(response) {
      if (response.ok) {
        return response.json();
      }
    }).catch(function(error) {
      console.log('Fallo la solicitud findByName ',error);
    });
  }

  findAll(){
    return this.interfaz.findAll().then(function(response) {
      if (response.ok) {
        return response.json();
      }
    }).catch(function(error) {
      console.log('Fallo la solicitud findAll ',error);
    });
  }

  findRange(first, pagesize){
    return this.interfaz.findRange(first, pagesize).then(function(response) {
      if (response.ok) {
        return response.json();
      }
    }).catch(function(error) {
      console.log('Fallo la solicitud findRange ',error);
    });
  }

  count(){
    return this.interfaz.count().then(function(response) {
      if (response.ok) {
        return response.json();
      }
    }).catch(function(error) {
      console.log('Fallo la solicitud count ',error);
    });
  }

  findById(id){
    return this.interfaz.findById(id).then(function(response) {
      if (response.ok) {
        return response.json();
      }
    }).catch(function(error) {
      console.log('Fallo la solicitud findById  ',error);
    });
  }

  edit(entidad){
    return this.interfaz.edit(entidad).then(function(response){
      if (response.ok) {
        return response.json();
      }
    }).catch(function(error){
      console.log("Fallos la solicitud edit");
    });
  }

}
export default AbstractController;
